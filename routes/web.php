<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



/* Admin Register */
Route::get('auth/register','AuthAdmin\RegisterController@showRegistrationForm')->name('get-register-admin');
Route::post('auth/register-admin','AuthAdmin\RegisterController@register')->name('post-register-admin');

/* Admin Login */
Route::get('auth','AuthAdmin\LoginController@showLoginForm')->name('get-login-admin');
Route::post('auth/post-login','AuthAdmin\LoginController@login')->name('login-admin');
Route::post('auth/logout','AuthAdmin\LoginController@logout')->name('post-logout-admin');


/*Dashboard*/
Route::get('dashboard','AdminController@index')->name('dashboard');


/*Services*/
Route::get('services','ServiceController@index')->name('services');
Route::post('services/new','ServiceController@store')->name('post-new-services');
Route::post('services/submit-skills','ServiceController@submitSkills')->name('post-submit-skills');
Route::post('post-drop-skills/{id}','ServiceController@destroy')->name('post-drop-skills');

/*Counties*/
Route::get('counties','CountyController@index')->name('counties');
Route::get('counties/create','CountyController@create')->name('counties/create');

/*Notifications*/
Route::get('notifications','NotificationController@index')->name('notifications');
/*Constituencies*/
Route::get('constituencies','ConstituencyController@index')->name('constituencies');
Route::post('constituencies/post-new-constituency','ConstituencyController@store')->name('post-new-constituency');

/*Wards*/
Route::get('wards','WardController@index')->name('wards');
Route::post('wards/post-new-ward','WardController@store')->name('post-new-ward');

/*Locations*/
Route::post('post-submit-locations','LocationController@store')->name('post-submit-locations');
Route::post('post-drop-location/{id}','LocationController@destroy')->name('post-drop-location');


/*Ajax Queries*/
Route::post('ajax/getCounties','CountyController@ajaxGetCounties')->name('ajax/getCounties');
Route::post('ajax/fetch-counties','CountyController@ajaxGetAllCounties')->name('ajax/fetch-counties');
Route::post('ajax/fetch-constituencies','ConstituencyController@ajaxGetAllConstituencies')->name('ajax/fetch-constituencies');
Route::post('ajax/fetch-wards','WardController@ajaxGetAllWards')->name('ajax/fetch-wards');

/*users*/
Route::get('users','UserController@index')->name('users');
Route::get('profile','UserController@getProfile')->name('profile');
Route::get('user/profile','UserController@getUserProfile')->name('user-profile');
Route::get('user/profile/{id}','UserController@getUserProfile')->name('get-user-profile');
Route::post('user/contact/{id}','UserController@update')->name('post-contact');







