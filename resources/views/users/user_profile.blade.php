@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
        	<div class="panel panel-default">
        		<div class="panel-body">
                    <img src="{{ url('images/jere.jpg')  }}" class="img-responsive " style="height;" alt="">
        		</div>
        	</div>

            <div class="panel panel-default">
            	<div class="panel-body">
            	   {{ $user->contacts }}
            	</div>
            </div>
        </div>

        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
        	<div class="row">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Services</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-group">
                            @foreach(\App\ServiceUser::where('user_id',$user->id)->get() as $service)
                                <li class="list-group-item"> <b>{{ $loop->iteration }}.</b> {{ \App\Service::find($service->service_id)->name }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
        	</div>


            <div class="row">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Locations</h3>
                    </div>
                    <div class="panel-body">
                        @foreach(\App\Location::where('user_id',$user->id)->take(3)->get() as $service)
                            <li class="list-group-item"> <b>{{ $loop->iteration }}.</b> {{ \App\County::find($service->county_id)->name }} - <small>{{ \App\Ward::find($service->ward_id)->name }}</small></li>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection