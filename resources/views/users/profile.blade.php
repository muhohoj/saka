@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3">
                <style>
                    .file-drop-area label {
                        display: block;
                        padding: 1em;
                        background: #eee;
                        text-align: center;
                        cursor: pointer;


                    }
                </style>
                <div class="panel panel-default" style="padding: 20px;background: none;">

                    <div class="form-group">
                        <div style="border-radius: 180px;" class="slim"
                             data-label="Click here to upload Profile Image"
                             data-size="640,640"
                             data-ratio="1:1">
                            <input id="image" onchange="upload()" type="file" name="image[]" required />

                        </div>

                    </div>
                    <ul class="list-group">
                    	<li class="list-group-item">{{ Auth::user()->name }} <a href="" class="pull-right">Edit</a></li>
                    	<li class="list-group-item">{{ Auth::user()->email }} <a href="" class="pull-right">Edit</a></li>

                    </ul>
                </div>

                <div class="panel panel-default">
                	<div class="panel-body">
                        <form autocomplete="off" action="{{ route('post-contact',Auth::user()->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="phone_number">Phone Number</label>
                                <input value="{{ Auth::user()->contacts }}" type="text" class="form-control" name="contacts" placeholder="+254...">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                	</div>
                </div>
            </div>

            <div class="col-xs-5 col-sm-5 col-md-6 col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <span class="badge">5</span>
                                Item 1
                            </li>
                            <li class="list-group-item">
                                <span class="badge">1</span>
                                Item 2
                            </li>
                            <li class="list-group-item">
                                <span class="badge">25</span>
                                Item 3
                            </li>
                        </ul>
                    </div>
                </div>
            </div>


            <div class="col-xs-5 col-sm-5 col-md-2 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function upload() {
            console.log("uploaded");
        }
    </script>
    @endsection