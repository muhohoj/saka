@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>
                    Constituencies
                    <button type="button" class="btn btn-primary pull-right bootstrap-modal-form-open" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                        Add New
                    </button>
                </h3>
            </div>
        </div>

        <div class="panel panel-default">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                @foreach($constituencies as $constituency)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $constituency->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>


        {{--Dialog to add new service--}}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <form action="{{ route('post-new-constituency') }}" method="post" autocomplete="off" class="bootstrap-modal-form">
                    {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">New Constituency</h4>
                        </div>
                        <div class="modal-body">
                            {{--Constituency--}}
                            <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                <label for="recipient-name" class="control-label">Name:</label>
                                <input name="name" type="text" class="form-control" id="name">
                                {!! $errors->first('county', '<p class="help-block">:message</p>') !!}
                            </div>

                            {{--Counties--}}
                            <div class="form-group {!! $errors->has('county') ? 'has-error' : '' !!}">
                                <label for="recipient-name" class="control-label">County:</label>
                                <select name="county" id="county" class="form-control">
                                    @foreach($counties as $county)
                                    <option value="{{ $county->id }}">{{ $county->name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('county', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{--End Dialog to add new service--}}
    </div>
    @endsection