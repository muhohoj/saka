@extends('layouts.app')
@section('content')
 <div class="container">
     {{--d3e0e9--}}

     <div class="panel panel-default">
         <div class="panel-body">
             <div class="">
                 <h3>
                     Counties
                     <a href="{{ route('counties/create') }}" class="btn btn-primary pull-right" >
                         Load Counties
                     </a>
                 </h3>
             </div>
         </div>
     </div>

     <div class="panel panel-default">
         <!-- Table -->
         <table class="table table-bordered table-responsive table-striped">
             <thead>
             <tr>
                 <th>#</th>
                 <th>County</th>
             </tr>
             </thead>
             <tbody>
             @foreach($counties as $county)
             <tr>
                 <td>{{ $loop->iteration }}</td>
                 <td>{{ $county->name }}</td>
             </tr>
             @endforeach
             </tbody>
         </table>
     </div>
     {{$counties}}
 </div>
    @endsection