<!doctype html>
<html lang="{{ config('app.locale') }}">
    @include('layouts.partials.header')
    <body style="padding-top: 70px;padding-bottom: 70px;">
        <div class="flex-center position-ref full-height">
            @include('layouts.partials.nav')
            <div class="container-fluid">
                <div class="jumbotron jumbo">
                    <div class="container">
                        <h1 class="text-center text-muted">{{ config('app.name') }}</h1>
                        <p class="text-muted text-center" style="color: #FFFFFF">The Services Hub</p>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="service">Service</label>
                                <select onchange="fetchCounties()" name="service" id="service" class="form-control">
                                    <option value="0"> -- Select Service -- </option>
                                    @foreach($services as $service)
                                    <option value="{{ $service->id }}"> {{ $service->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="service">County</label>
                                <select onchange="fetchConstituencies()" name="county" id="county" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="service">Constituency</label>
                                <select onchange="fetchWards()" name="constituency" id="constituency" class="form-control"></select>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="service">Ward</label>
                                <select  name="ward" id="ward" class="form-control"></select>
                            </div>
                        </div>


                    </div>
                </div>


                <div class="container-fluid" style="height: 400px;background:none;">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            {{--<div class="panel panel-default">--}}
                                {{--<div class="panel-body">--}}
                                    {{--<ul class="list-group">--}}
                                        {{--@foreach($counties as $county)--}}
                                            {{--<li class="list-group-item">--}}
                                                {{--<a href="">{{ $county->name }}</a>--}}
                                            {{--</li>--}}
                                        {{--@endforeach--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        {{--Services--}}
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-8">
                            @foreach($users as $user)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3><span class="text-muted">{{ ucwords($user->name) }}</span>
                                        <small></small> <small class="pull-right"><a href="{{ route('get-user-profile',$user->id) }}" class="text-left">Profile</a></small></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <small>{{$user->email}}</small>
                                            </div>
                                        	<div class="panel-body">
                                                <img  src="{{ url('images/jere.jpg') }}" class="img-responsive img-circle pull-left" style="height: 100px;margin:auto 0!important;">
                                        	</div>
                                            <div class="panel-footer">
                                                <h6> Services  <div class="pull-left badge">{{ count(\App\ServiceUser::where('user_id',$user->id)->get()) }}</div></h6>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-9">
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                Services
                                            </div>
                                        	<div class="panel-body">
                                                <ul class="list-group">
                                                    @foreach(\App\ServiceUser::where('user_id',$user->id)->take(3)->get() as $service)
                                                        <li class="list-group-item"> <b>{{ $loop->iteration }}.</b> {{ \App\Service::find($service->service_id)->name }}</li>
                                                    @endforeach
                                                </ul>
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer">
                                    <p>Registered companies should be able to create employer accounts which they can use to create job posts which they pay</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>




                </div>
            </div>
        </div>

        <script>
            function fetchConstituencies() {
                $("#constituency").empty();
                var county = $("#county").val();
                jsonData = {'county':county};
                $.ajax({
                    url:"{{ route('ajax/fetch-constituencies') }}",
                    method:'POST',
                    dataType:'TEXT',
                    data:jsonData,
                    success:function (response) {
                        if(response != ""){
                            $("#constituency").append(
                                '<option value="0">' + "Select Constituency" +'</option>'
                            ).css('border','1px solid green');

                            /*loop through the data object */
                            $.each(JSON.parse(response), function(index, val) {
                                $("#constituency")
                                    .append(
                                        '<option value="' + val.id +'">' + val.name +'</option>'
                                    ).css('text-transform','capitalize');
                            });
                        }else{

                            alert("This has no rooms");

                        }
                    }
                });
            }


            function fetchWards() {
                var constituency = $("#constituency").val();
                var jsonData = {'constituency':constituency};
                $.ajax({
                    url:"{{ route('ajax/fetch-wards') }}",
                    method:'POST',
                    dataType:'TEXT',
                    data:jsonData,
                    success:function (response) {
                       if(response!=""){
                          $("#ward").append(
                              '<option value=""> -- Select Ward -- </option>'
                          ).css('text-transform','1px solid yellow');

                          /*loop through the data object*/
                          $.each(JSON.parse(response),function(index, val) {
                              $("#ward").append(
                                  '<option value="' + val.id +'">' + val.name +'</option>'
                              );
                          });
                       }
                    }
                });

            }
            
            function fetchCounties() {
                $("#county").empty();
                $("#constituency").empty();
                var service = $("#service").val();
                var jsonData = {'service':service};
                $.ajax({
                    url:"{{ route('ajax/fetch-counties') }}",
                    method:'POST',
                    dataType: 'TEXT',
                    data:jsonData,
                    success:function (response) {
                        if(response != ""){
                            $("#county").append(
                                '<option value="0">' + "Select County" +'</option>'
                            ).css('border','1px solid green');

                            /*loop through the data object */
                            $.each(JSON.parse(response), function(index, val) {
                                $("#county")
                                    .append(
                                        '<option value="' + val.id +'">' + val.name +'</option>'
                                    ).css('text-transform','capitalize');
                            });
                        }else{

                            alert("This has no rooms");

                        }
                    }
                });
            }
        </script>
   <footer class="navbar navbar-inverse navbar-fixed-bottom" role="navigation" style="height: 40px;">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        			<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        		</button>
        		<a class="navbar-brand" href="#">{{ config('app.name') }}</a>
        	</div>
        
        	<!-- Collect the nav links, forms, and other content for toggling -->
        	<div class="collapse navbar-collapse navbar-ex1-collapse">
        		<ul class="nav navbar-nav">
        			<li class="active"><a href="{{ url('auth') }}">Admin</a></li>
        		</ul>

        		</div><!-- /.navbar-collapse -->
        </footer>
    </body>
</html>
