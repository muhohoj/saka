@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>
                    Wards
                    <button type="button" class="btn btn-primary pull-right bootstrap-modal-form-open" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                        Add New
                    </button>
                </h3>
            </div>
        </div>


        <div class="panel panel-default">
        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th>#</th>
        				<th>Ward Name</th>
        			</tr>
        		</thead>
        		<tbody>
                @foreach($wards as $ward)
        			<tr>
        				<td>{{ $loop->iteration }}</td>
        				<td>{{ $ward->name }}</td>
        			</tr>
                 @endforeach
        		</tbody>
        	</table>
        </div>
        {{--Dialog to add new service--}}
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <form action="{{ route('post-new-ward') }}" method="post" autocomplete="off" class="bootstrap-modal-form">
                    {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">New Constituency</h4>
                        </div>
                        <div class="modal-body">
                            {{--Counties--}}
                            <div class="form-group {!! $errors->has('county') ? 'has-error' : '' !!}">
                                <label for="recipient-name" class="control-label">County:</label>
                                <select onchange="ajaxFetch()" name="county" id="county" class="form-control">
                                    <option value="0">--Select County--</option>
                                    @foreach($counties as $county)
                                        <option value="{{ $county->id }}">{{ $county->name }}</option>
                                    @endforeach
                                </select>
                                {!! $errors->first('county', '<p class="help-block">:message</p>') !!}
                            </div>

                            {{--Constituencies--}}
                            <div class="form-group {!! $errors->has('constituency') ? 'has-error' : '' !!}">
                                <label for="recipient-name" class="control-label">Constituency:</label>
                                <select name="constituency" id="constituency" class="form-control">
                                </select>
                                {!! $errors->first('constituency', '<p class="help-block">:message</p>') !!}
                            </div>


                            {{--Name--}}
                            <div class="form-group {!! $errors->has('ward') ? 'has-error' : '' !!}">
                                <label for="ward" class="control-label">Name:</label>
                                <input name="ward" type="text" class="form-control" id="name">
                                {!! $errors->first('ward', '<p class="help-block">:message</p>') !!}
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{--End Dialog to add new service--}}
    </div>
    <script !src="">
        function ajaxFetch() {
            $("#constituency").empty();
            $("#ward").empty();
            var county = $("#county").val();
            var data = {county:county};
            $.ajax({
                url: "{{ route('ajax/getCounties') }}",
                type: 'POST',
                dataType: 'TEXT',
                data:data,
                success:function (data) {
                    if(data != ""){
                        $("#constituency").append(
                            '<option value="0">' + "Select Constituency" +'</option>'
                        ).css('border','1px solid green');
                        $.each(JSON.parse(data), function(index, val) {
                            $("#constituency")
                                .append(
                                    '<option value="' + val.id +'">' + val.name +'</option>'
                                ).css('text-transform','capitalize');
                        });
                    }else{

                        alert("This has no rooms");

                    }
                }
            });
        }
    </script>
@endsection