@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>
                    Services
                    <button type="button" class="btn btn-primary pull-right bootstrap-modal-form-open" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">
                        Add New
                    </button>
                </h3>
            </div>
        </div>
        {{-- Table--}}
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th colspan="2" style="text-align: center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $service->name }}</td>
                            <td>
                                <button type="button" class="btn btn-warning  bootstrap-modal-form-open" data-toggle="modal" data-target="#editModal{{$service->id}}" data-whatever="@mdo">
                                    Edit
                                </button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-danger  bootstrap-modal-form-open" data-toggle="modal" data-target="#editModal{{$service->id}}" data-whatever="@mdo">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        {{--Edit Dialog to add new service--}}
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <form action="{{ route('post-new-services') }}" method="post" autocomplete="off" class="bootstrap-modal-form">
                    {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">New Service</h4>
                        </div>
                        <div class="modal-body">

                                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                    <label for="recipient-name" class="control-label">Name:</label>
                                    <input name="name" type="text" class="form-control" id="name">
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{--End  Edit Dialog to add new service--}}




        {{--Dialog to add new service--}}
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <form action="{{ route('post-new-services') }}" method="post" autocomplete="off" class="bootstrap-modal-form">
                    {{ csrf_field() }}
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">New Service</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                <label for="recipient-name" class="control-label">Name:</label>
                                <input name="name" type="text" class="form-control" id="name">
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        {{--End Dialog to add new service--}}
    </div>
    @endsection