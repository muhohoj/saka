@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel panel-default panel-danger">
            <div class="panel-body">
                <h3 class="text-center text-muted">
                    Skills And Locations
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="text-center text-muted">
                            Select Your Skills
                        </h4>
                    </div>
                    <form action="{{ route('post-submit-skills') }}" method="post">
                        {{ csrf_field() }}
                    <div class="panel-body">
                        <ul class="list-group">
                            @foreach($services  as $service)
                                <li class="list-group-item">
                                        {{ $loop->iteration }}. {{ $service->name }}
{{--                                    {{ DB::table('service_user')->where('service_id',$service->id)->where('user_id',Auth::user()->id)->get() }}--}}
                                    <input type="checkbox" value="{{ $service->id }}" name="services[]" class="pull-right" id="services">
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>


            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-3">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="text-center text-muted">
                            Define Your Locations
                        </h4>
                    </div>
                    <form action="{{ route('post-submit-locations') }}" method="post" id="location">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="county">County</label>
                                <select onchange="fetchConstituencies()"  name="county" id="county" class="form-control">
                                    @foreach($counties as $county)
                                        <option value="{{ $county->id }}"> {{ $county->name }} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="constituency">Constituency</label>
                                <select onchange="fetchWards()" name="constituency" id="constituency" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label for="ward">Ward</label>
                                <select onchange="save()" name="ward" id="ward" class="form-control"></select>
                            </div>
                        </div>
                      </form>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h4 class="text-center text-muted">
                           <p style="color: #FFFFFF">Locations</p>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Location</th>
                                <th>Drop</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($my_locations as $my_location)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{  ucwords(\App\Http\Controllers\WardController::getWard($my_location->ward_id)) }}
                                    <small class="pull-right text-primary">({{ \App\Http\Controllers\CountyController::getCounty($my_location->county_id)}})</small>
                                </td>
                                <td>
                                    <form action="{{ route('post-drop-location',$my_location->id) }}" method="POST" role="form">
                                        {{ csrf_field() }}
                                        <button type="submit" style="color: red" class="close text-danger" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </form>
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="text-center text-muted">
                            My Skills
                        </h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Skill</th>
                                <th>Drop</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mySkills as $skills)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ App\Service::find($skills->service_id)->name }}</td>
                                    <td>
                                        <form action="{{ route('post-drop-skills',$skills->id) }}" method="POST" role="form">
                                            {{ csrf_field() }}
                                            <button type="submit" class="close text-danger" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function save() {
            $("#location").submit();
        }
        function fetchConstituencies() {
            $("#constituency").empty();
            var county = $("#county").val();
            jsonData = {'county':county};
            $.ajax({
                url:"{{ route('ajax/fetch-constituencies') }}",
                method:'POST',
                dataType:'TEXT',
                data:jsonData,
                success:function (response) {
                    if(response != ""){
                        $("#constituency").append(
                            '<option value="0">' + "Select Constituency" +'</option>'
                        ).css('border','1px solid green');
                        $.each(JSON.parse(response), function(index, val) {
                            $("#constituency")
                                .append(
                                    '<option value="' + val.id +'">' + val.name +'</option>'
                                ).css('text-transform','capitalize');
                        });
                    }else{

                        alert("This has no rooms");

                    }
                }
            });
        }


        function fetchWards() {
            var constituency = $("#constituency").val();
            $("#ward").empty();
            var jsonData = {'constituency':constituency};
            $.ajax({
                url:"{{ route('ajax/fetch-wards') }}",
                method:'POST',
                dataType:'TEXT',
                data:jsonData,
                success:function (response) {
                    if(response!=""){
                        $("#ward").append(
                            '<option value=""> -- Select Ward -- </option>'
                        ).css('text-transform','1px solid yellow');
                        /*loop through the data object*/
                        $.each(JSON.parse(response),function(index, val) {
                            $("#ward").append(
                                '<option value="' + val.id +'">' + val.name +'</option>'
                            ).css('border','1px solid green');
                        });
                    }
                }
            });

        }

        function fetchCounties() {
            $("#county").empty();
            $("#constituency").empty();
            var service = $("#service").val();
            var jsonData = {'service':service};
            $.ajax({
                url:"{{ route('ajax/fetch-counties') }}",
                method:'POST',
                dataType: 'TEXT',
                data:jsonData,
                success:function (response) {
                    if(response != ""){
                        $("#county").append(
                            '<option value="0">' + "Select County" +'</option>'
                        ).css('border','1px solid green');

                        /*loop through the data object */
                        $.each(JSON.parse(response), function(index, val) {
                            $("#county")
                                .append(
                                    '<option value="' + val.id +'">' + val.name +'</option>'
                                ).css('text-transform','capitalize');
                        });
                    }else{

                        alert("This has no rooms");

                    }
                }
            });
        }
    </script>
    @endsection