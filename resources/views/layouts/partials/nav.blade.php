<nav class="navbar navbar-inverse navbar-fixed-top" style="">
    <div class="container">
        <div class="navbar-header">

            <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- Branding Image -->
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav">
                &nbsp;
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @if(Auth::guard('admin')->check())
                    <li class="{{ Request::is('dashboard*')? 'active':'null' }}"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li  class="{{ Request::is('users*')? 'active':'null' }}">
                        <a href="{{ route('users') }}">
                            Users <span class="badge badge-success">{{ $count_users }}</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('services*')? 'active':'null' }}">
                        <a href="{{ route('services') }}">
                            Services <span class="badge badge-orange">{{ $count_services }}</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('counties*')? 'active':'null' }}">
                        <a href="{{ route('counties') }}">
                            County
                            <span class="badge badge-tomato">{{ $count_counties }}</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('constituencies*')? 'active':'null' }}">
                        <a href="{{ route('constituencies') }}">
                            Constituency
                            <span class="badge badge-maroon">{{ $count_constituencies }}</span>
                        </a>
                    </li>
                    <li class="{{ Request::is('wards*')? 'active':'null' }}">
                        <a href="{{ route('wards') }}">
                            Ward
                            <span class="badge badge-purple">{{ $count_wards }}</span>
                        </a>
                    </li>
                    <li class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="">Settings</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('post-logout-admin') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>

                @elseif (Auth::guest())

                    <li class="{{ Request::is('login*')? 'active':'null' }}"><a href="{{ route('login') }}">Login</a></li>
                    <li class="{{ Request::is('register*')? 'active':'null' }}"><a href="{{ route('register') }}">Register</a></li>
                @else
                    <li class="{{ Request::is('home*') ? 'active':'null' }}"><a href="{{ url('home') }}">Dashboard</a></li>
                    <li class="{{ Request::is('services*') ? 'active':'null' }}"><a href="{{ route('services') }}">Services</a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Notifications <sup><span class="badge" style="background: red;">3</span></sup> <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="">Steve <sup><span class="badge" style="background: tomato;">1</span></sup></a></li>
                            <li class="divider"></li>
                            <li><a href="">John <sup><span class="badge" style="background: tomato;">1</span></sup></a></li>
                            <li class="divider"></li>
                            <li><a href="">Mary <sup><span class="badge" style="background: tomato;">1</span></sup></a></li>
                        </ul>
                    </li>
                    {{--<li class="{{ Request::is('notifications*') ? 'active':'null' }}">--}}
                        {{--<a href="{{ route('notifications') }}">--}}
                            {{--Notifications --}}
                        {{--</a>--}}
                    {{--</li>--}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('profile') }}">Profile</a></li>
                            <li class="divider"></li>
                            <li><a href="">Settings</a></li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>