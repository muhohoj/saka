<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('layouts.partials.header')
<body style="padding-top: 70px;">
    <div id="app">
        @include('layouts.partials.nav')
        @yield('content')
    </div>
    @include('layouts.partials.scripts')
</body>
</html>
