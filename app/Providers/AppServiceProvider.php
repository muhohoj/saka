<?php

namespace App\Providers;

use App\Constituency;
use App\Service;
use App\User;
use App\Ward;
use Illuminate\Support\ServiceProvider;
use \Illuminate\Support\Facades\View;
use App\County;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::share(
            [
                'count_counties'=>County::count(),
                'count_services'=>Service::count(),
                'count_constituencies'=>Constituency::count(),
                'count_users'=>User::count(),
                'count_wards'=>Ward::count(),
            ]
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
