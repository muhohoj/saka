<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //

    protected $fillable  = [
        'name'
    ];


    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }


    public function user(){
        return $this->belongsToMany('App\User');
    }
}
