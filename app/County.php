<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{

    //
    protected $fillable = ['name'];


    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return
     */
    public function getNameAttribute($value)
    {
        return ucwords(substr($value,0,-7));
    }


    public function user(){
        return $this->belongsTo('App\User');
    }
}
