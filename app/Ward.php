<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    //

    protected $fillable = [
        'name','county_id','constituency_id'
    ];
}
