<?php

namespace App\Http\Controllers;

use App\Service;
use App\ServiceUser;
use Illuminate\Http\Request;
use Auth;
class ServiceController extends Controller
{
    protected static $services;
    protected static $user;

    public function __construct()
    {
        self::$services = self::getAllServices();

//        self::$user = Auth::user()->id;
    }


    public static function getMySkills(){
        return ServiceUser::where('user_id',Auth::user()->id)->get();
    }

    public static function getAllServices(){
        return Service::all();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*Check the Session Accessing*/
        if(Auth::user()){
            $data = [
                'services'=>self::getAllServices(),
                'mySkills'=>self::getMySkills(),
                'counties'=>CountyController::getCounties(),
                'my_locations'=>LocationController::getUserLocations(),
            ];
            return view('services.get_services',$data);
        }else if(Auth::guard('admin')){
            $data = ['services'=>self::getAllServices()];
            return view('services.index',$data);
        }
    }
    public function submitSkills(Request $request){
        $this->validate($request, [
            'services' => 'required',
        ]);
        $services = $request->services;
        $user = Auth::id();
        foreach ($services as $service){
            ServiceUser::firstOrcreate(
                ['service_id'=>$service,'user_id'=>$user],
                ['service_id'=>$service,'user_id'=>$user]
            );
        }

        return redirect()->route('services');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:services|max:255',
        ]);
        $name = trim($request->name);
        Service::create(['name'=>$name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $skill = ServiceUser::find($id);
        $skill->delete($id);
        return redirect()->back()->with('status','Success a service has been deleted');
    }
}
