<?php

namespace App\Http\Controllers;

use App\Constituency;
use App\Ward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class WardController extends Controller
{
    public static function getWard($ward_id){
        return Ward::find($ward_id)->name;
    }

    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public static function getAllWards(){
        return Ward::all();
    }

    public function ajaxGetAllWards(Request $request){
        $constituency = $request->constituency;
        return $wards = Ward::where('constituency_id',$constituency)->get();



    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = [
            'counties'=>CountyController::getCounties(),
            'wards'=>self::getAllWards()
        ];
        return view('wards.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'county' => 'required|max:255',
            'constituency' => 'required|max:255',
            'ward' => 'required|max:255',
        ]);

        Ward::firstOrcreate(
            ['county_id'=>$request->county,'constituency_id'=>$request->constituency],
            ['county_id'=>$request->county,'constituency_id'=>$request->constituency,'name'=>$request->ward]
        );

        return redirect()->route('wards');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
