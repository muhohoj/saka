<?php

namespace App\Http\Controllers;

use App\Constituency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
class ConstituencyController extends Controller
{

    public static function getAllConstituencies(){
        return Constituency::all();
    }

    public function ajaxGetAllConstituencies(Request $request){
        $county = $request->county;
        $constituencies = Constituency::where('county_id',$county)->get();
        return $constituencies;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*fetch Counties from the Database and pass to the view*/
        $data = [
            'counties'=>CountyController::getCounties(),
            'constituencies'=>self::getAllConstituencies(),
        ];
        return view('constituencies.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'county' => 'required|max:255',
            'name' => 'required|unique:constituencies|max:255',
        ]);
        $name = $request->name;
        $county = $request->county;
        Constituency::create(
            ['name'=>$name,'county_id'=>$county],['name'=>$name,'county_id'=>$county]
        );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
