<?php

namespace App\Http\Controllers;

use App\Constituency;
use App\County;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class CountyController extends Controller
{

    public static function getCounty($county_id){
        return County::find($county_id)->name;
    }

    /*Get All Counties*/
    public static function getCounties(){
        return County::all();
    }

    /*Get All Counties*/
    public static function getPaginatedCounties(){
        return County::paginate(5);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['counties'=>self::getPaginatedCounties()];
        return view('counties.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $counties_in_kenya = array(
            "Baringo County",
            "Bomet County",
            "Bungoma County",
            "Busia County",
            "Elgeyo Marakwet County",
            "Embu County",
            "Garissa County",
            "Homa Bay County",
            "Isiolo County",
            "Kajiado County",
            "Kakamega County",
            "Kericho County",
            "Kiambu County",
            "Kilifi County",
            "Kirinyaga County",
            "Kisii County",
            "Kisumu County",
            "Kitui County",
            "Kwale County",
            "Laikipia County",
            "Lamu County",
            "Machakos County",
            "Makueni County",
            "Mandera County",
            "Meru County",
            "Migori County",
            "Marsabit County",
            "Mombasa County",
            "Muranga County",
            "Nairobi County",
            "Nakuru County",
            "Nandi County",
            "Narok County",
            "Nyamira County",
            "Nyandarua County",
            "Nyeri County",
            "Samburu County",
            "Siaya County",
            "Taita Taveta County",
            "Tana River County",
            "Tharaka Nithi County",
            "Trans Nzoia County",
            "Turkana County",
            "Uasin Gishu County",
            "Vihiga County",
            "Wajir County",
            "West Pokot County",
        );
        foreach ($counties_in_kenya as $county){
            County::firstOrcreate(['name'=>$county],['name'=>$county]);
        }
        return redirect()->route('counties');
    }

    public function ajaxGetAllCounties(){
        $counties = County::all();
        return Response::json($counties);
    }


    public function ajaxGetCounties(Request $request){
        $county = $request->county;
        $results = Constituency::where('county_id',$county)->get();
        return Response::json($results);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
