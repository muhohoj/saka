<?php

namespace App\Http\Controllers;

use App\ServiceUser;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public static function getAllUsers(){
        return User::all();
    }
    public function getProfile(){
        return view('users.profile');
    }

    /*save contact*/
    public function saveContact(Request $request){

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = ['users'=>Self::getAllUsers()];
        return view('users.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function getUserProfile($id){
        $user = User::findOrFail($id);
        $user_services = ServiceUser::where('user_id',$user->id)->get();
        $data = [
            'user'=>$user,
            'services'=>$user_services
        ];
        return view('users.user_profile',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = $request->contacts;
        $user = User::find($id);
        $user->contacts = $contact;
        $user->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
