<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    //

    protected $fillable  = ['county_id','constituency_id','ward_id','user_id'];


    public function user(){
        return $this->belongsTo('App\User');
    }
}
