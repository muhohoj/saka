<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Constituency extends Model
{
    //

    protected $fillable = [
        'county_id','name'
    ];

    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return void
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtolower($value);
    }


    /**
     * Set the user's first name.
     *
     * @param  string  $value
     * @return
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }
}
